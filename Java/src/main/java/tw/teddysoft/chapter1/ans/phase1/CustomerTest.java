package tw.teddysoft.chapter1.ans.phase1;

import org.approvaltests.Approvals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CustomerTest {

    @Before
    public void setup(){


    }

    @Test
    public void testStatement(){

        Customer customer = new Customer("Teddy");
        customer.addRental(new Rental(new Movie("美國隊長", Movie.REGULAR), 1));
        customer.addRental(new Rental(new Movie("功夫熊貓", Movie.CHILDREN), 15));
        customer.addRental(new Rental(new Movie("泰迪之夜", Movie.NEW_RELEASE), 5));

        customer.addRental(new Rental(new Movie("羅馬假期", Movie.REGULAR), 3));
        customer.addRental(new Rental(new Movie("小熊維尼", Movie.CHILDREN), 2));
        customer.addRental(new Rental(new Movie("法律女王", Movie.NEW_RELEASE), 1));

        Approvals.verify(customer.statement());
    }


    // achieve 100% branch
    @Ignore
    public void testStatement_step3(){

        Customer customer = new Customer("Teddy");
        customer.addRental(new Rental(new Movie("美國隊長", Movie.REGULAR), 1));
        customer.addRental(new Rental(new Movie("功夫熊貓", Movie.CHILDREN), 15));
        customer.addRental(new Rental(new Movie("泰迪之夜", Movie.NEW_RELEASE), 5));

        customer.addRental(new Rental(new Movie("羅馬假期", Movie.REGULAR), 3));
        customer.addRental(new Rental(new Movie("小熊維尼", Movie.CHILDREN), 2));
        customer.addRental(new Rental(new Movie("法律女王", Movie.NEW_RELEASE), 1));

        Approvals.verify(customer.statement());
    }


    // see the coverage at Customer.statement method and
    // try to increase the test coverage
    @Ignore
    public void testStatement_step2(){

        Customer customer = new Customer("Teddy");
        customer.addRental(new Rental(new Movie("美國隊長", Movie.REGULAR), 1));
        customer.addRental(new Rental(new Movie("功夫熊貓", Movie.CHILDREN), 15));
        customer.addRental(new Rental(new Movie("泰迪之夜", Movie.NEW_RELEASE), 5));

        Approvals.verify(customer.statement());
    }



    @Ignore
    public void testStatement_step1(){

        Customer customer = new Customer("Teddy");
        Rental rental = new Rental(new Movie("美國隊長", 0), 5);
        customer.addRental(rental);

//        assertEquals("", customer.statement());
//        Rental record for Teddy
//        美國隊長	6.5
//        Amount owed is 6.5
//        You earned 1 frequent renter points

        Approvals.verify(customer.statement());
    }
}
