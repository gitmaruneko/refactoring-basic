/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ShotgunSurgery.InlineClass.exercise;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Course {
	private List<Student> _students = new LinkedList<>();
	private Teacher _teacher;
	
	public void setTeacher(Teacher arg){
		_teacher = arg;
	}
	public Teacher getTeacher(){
		return _teacher;
	}
	
	public void addStudent(Student arg){
		_students.add(arg);
	}
	
	public List<Student> getStudents(){
		return Collections.unmodifiableList(_students);
	}
	

	// a lot of code
	
	public boolean save(String dbDriverName,  String dbURL, String dbUserName, String dbPassword){
		TeacherSaveCommand teacher = new TeacherSaveCommand(dbDriverName, dbURL, dbUserName, dbPassword);
		if (!teacher.save(_teacher))
			return false;
		
		StudentSaveCommand student = new StudentSaveCommand(dbDriverName, dbURL, dbUserName, dbPassword);
		if (!student.save(this.getStudents()))
			return false;
		
		return new CourseSaveCommand(dbDriverName, dbURL, dbUserName, dbPassword).save(this);
	}
	
	
	
	
}
