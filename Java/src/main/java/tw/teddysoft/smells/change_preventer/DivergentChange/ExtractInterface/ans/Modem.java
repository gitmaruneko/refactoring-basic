/*
 * Adapted from Robert C. Martin's Agile Software Development book
 */
package tw.teddysoft.smells.change_preventer.DivergentChange.ExtractInterface.ans;

public class Modem implements Connection, DataChannel {

	@Override
	public void send(char c) {
	}

	@Override
	public char recv() {
		return 0;
	}

	@Override
	public void dial(String number) {
	}

	@Override
	public void hangup() {
	}
}
