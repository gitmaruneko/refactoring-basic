/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ParallelInheritanceHierarchies.MoveMethod_MoveField.ans;

public abstract class Notebook {
	private String _manufacturer;
	private String _serialNumber;
	
	public void setManufacturer(String arg){
		_manufacturer = arg;
	}
	
	public String getManufacturer(){
		return _manufacturer;
	}
	
	
	public void setSerialNumber(String arg){
		_serialNumber = arg;
	}
	
	public String getSerialNumber(){
		return _serialNumber;
	}
	
	abstract String getName();
	
	public abstract String toJson();
}
