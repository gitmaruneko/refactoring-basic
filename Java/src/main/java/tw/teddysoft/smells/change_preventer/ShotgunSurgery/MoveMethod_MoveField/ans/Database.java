/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ShotgunSurgery.MoveMethod_MoveField.ans;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
	private String _dbDriverName;
	private String _dbURL;
	private String _dbUserName;
	private String _dbPassword;
	
	public Database(){
		
	}
	
	public Database(String dbDriverName, String dbURL, String dbUserName, String dbPassword){
		_dbDriverName = dbDriverName;
		_dbURL = dbURL;
		_dbUserName = dbUserName;
		_dbPassword = dbPassword;
	}
	
	public void setDBDriveName(String arg){
		_dbDriverName = arg;
	}

	public void setDBURL(String arg){
		_dbURL = arg;
	}
	
	public void setDBUserName(String arg){
		_dbUserName = arg;
	}
	
	public void setDBPassword(String arg){
		_dbPassword = arg;
	}
	
	public boolean save(Course course){
		Connection conn = null;
		
		try
		{
		  Class.forName(_dbDriverName).newInstance();
		  String url = _dbURL;
		  conn = DriverManager.getConnection(url, _dbUserName, _dbPassword);
		  // .. save this to database
		}
		catch (Exception e) {
			System.out.println(e);
			return false;
		}
		finally{
			try {
				if(null != conn)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public boolean save(Student student){
		Connection conn = null;
		try
		{
		  Class.forName(_dbDriverName).newInstance();
		  String url = _dbURL;
		  conn = DriverManager.getConnection(url, _dbUserName, _dbPassword);
		  // .. save this to database
		}
		catch (Exception e) {
			System.out.println(e);
			return false;
		}
		finally{
			try {
				if(null != conn)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	
	public boolean save(Teacher teacher){
		Connection conn = null;
		try
		{
		  Class.forName(_dbDriverName).newInstance();
		  String url = _dbURL;
		  conn = DriverManager.getConnection(url, _dbUserName, _dbPassword);
		  // .. save this to database
		}
		catch (Exception e) {
			System.out.println(e);
			return false;
		}
		finally{
			try {
				if(null != conn)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	
	
}
