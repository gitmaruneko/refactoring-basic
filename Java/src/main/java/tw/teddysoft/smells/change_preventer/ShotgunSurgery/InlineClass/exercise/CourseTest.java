/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ShotgunSurgery.InlineClass.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class CourseTest {

	@Test
	public void demo_a_client_of_Course() {
		Course course = new Course();
		// a lot of code
		assertTrue(course.save("org.apache.derby.jdbc.ClientDriver", "jdbc:derby:testdb;create=true", "teddy", "ao1xx"));
	}
}
