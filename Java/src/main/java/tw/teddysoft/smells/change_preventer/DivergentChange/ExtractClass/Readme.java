/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.DivergentChange.ExtractClass;

import tw.teddysoft.common.annotation.See;
import tw.teddysoft.smells.bloater.DataClumps.ExtractClass_PreserveWholeObject.exercise.Teacher;
import tw.teddysoft.smells.bloater.DataClumps.ExtractClass_PreserveWholeObject.exercise.Course;
import tw.teddysoft.smells.bloater.DataClumps.ExtractClass_PreserveWholeObject.exercise.Student;

@See({Course.class, Student.class, Teacher.class})
public interface Readme {
}
