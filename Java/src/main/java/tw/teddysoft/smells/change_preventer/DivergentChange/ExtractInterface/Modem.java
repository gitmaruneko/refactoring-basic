/*
 * Adapted from Robert C. Martin's Agile Software Development book
 */
package tw.teddysoft.smells.change_preventer.DivergentChange.ExtractInterface;

public interface Modem {
	public void dial(String number);
	public void hangup();
	public void sned(char c);
	public char recv();
}
