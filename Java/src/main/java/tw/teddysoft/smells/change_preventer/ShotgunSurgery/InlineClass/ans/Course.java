/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ShotgunSurgery.InlineClass.ans;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Course {
	private List<Student> _students = new LinkedList<>();
	private Teacher _teacher;
	
	private String _dbDriverName;
	private String _dbURL;
	private String _dbUserName;
	private String _dbPassword;
	
	public void setTeacher(Teacher arg){
		_teacher = arg;
	}
	public Teacher getTeacher(){
		return _teacher;
	}
	
	public void addStudent(Student arg){
		_students.add(arg);
	}
	
	public List<Student> getStudents(){
		return Collections.unmodifiableList(_students);
	}
	

	// a lot of code
	
	public boolean save(String dbDriverName,  String dbURL, String dbUserName, String dbPassword){
		_dbDriverName = dbDriverName;
		_dbURL = dbURL;
		_dbUserName = dbUserName;
		_dbPassword = dbPassword;
		
		if (!saveTeacher(_teacher))
			return false;
		
		if (!saveStudents(this.getStudents()))
			return false;
		
		return saveCourse(this);
	}
	
	private boolean saveTeacher(Teacher teacher){
		Connection conn = null;
		
		try
		{
		  Class.forName(_dbDriverName).newInstance();
		  String url = _dbURL;
		  conn = DriverManager.getConnection(url, _dbUserName, _dbPassword);
		  // .. save this to database
		}
		catch (Exception e) {
			System.out.println(e);
			return false;
		}
		finally{
			try {
				if(null != conn)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	private boolean saveStudent(Student student){
		Connection conn = null;
		
		try
		{
		  Class.forName(_dbDriverName).newInstance();
		  String url = _dbURL;
		  conn = DriverManager.getConnection(url, _dbUserName, _dbPassword);
		  // .. save this to database
		}
		catch (Exception e) {
			System.out.println(e);
			return false;
		}
		finally{
			try {
				if(null != conn)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	private boolean saveStudents(List<Student> students){
		// a lot of code here
		return true;
	}
	
	
	private boolean saveCourse(Course course){
		Connection conn = null;
		
		try
		{
		  Class.forName(_dbDriverName).newInstance();
		  String url = _dbURL;
		  conn = DriverManager.getConnection(url, _dbUserName, _dbPassword);
		  // .. save this to database
		}
		catch (Exception e) {
			System.out.println(e);
			return false;
		}
		finally{
			try {
				if(null != conn)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}
