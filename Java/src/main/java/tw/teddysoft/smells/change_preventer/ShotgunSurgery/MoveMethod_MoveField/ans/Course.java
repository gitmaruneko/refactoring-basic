/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ShotgunSurgery.MoveMethod_MoveField.ans;

public class Course {
	private Database _database = new Database();
	
	public void setDBDriveName(String arg){
		_database.setDBDriveName(arg);
	}

	public void setDBURL(String arg){
		_database.setDBURL(arg);
	}
	
	public void setDBUserName(String arg){
		_database.setDBUserName(arg);
	}
	
	public void setDBPassword(String arg){
		_database.setDBPassword(arg);
	}
	
	// a lot of code
	
	public boolean save(){
		return _database.save(this);
	}
}


