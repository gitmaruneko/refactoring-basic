/*
 * Adapted from Robert C. Martin's Agile Software Development book
 */
package tw.teddysoft.smells.change_preventer.DivergentChange.ExtractInterface.ans;

public interface Connection {
	public void dial(String number);
	public void hangup();
}
