/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ShotgunSurgery.MoveMethod_MoveField.exercise;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Teacher {

	private String _dbDriverName;
	private String _dbURL;
	private String _dbUserName;
	private String _dbPassword;
	
	public Teacher(String dbDriverName, String dbURL, String dbUserName, String dbPassword){
		_dbDriverName = dbDriverName;
		_dbURL = dbURL;
		_dbUserName = dbUserName;
		_dbPassword = dbPassword;
	}
	
	
	// a lot of code
	
	public boolean save(){
		Connection conn = null;
		
		try
		{
		  Class.forName(_dbDriverName).newInstance();
		  String url = _dbURL;
		  conn = DriverManager.getConnection(url, _dbUserName, _dbPassword);
		  // .. save this to database
		}
		catch (Exception e) {
			System.out.println(e);
			return false;
		}
		finally{
			try {
				if(null != conn)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
}
