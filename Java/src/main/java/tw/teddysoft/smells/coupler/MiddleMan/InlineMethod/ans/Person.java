/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MiddleMan.InlineMethod.ans;

import org.junit.Assert;

public class Person {
	private String _firstName;
	private String _middleName;
	private String _lastName;

	public Person(String firstName, String middleName, String lastName){
		Assert.assertNotNull(firstName);
		Assert.assertNotNull(lastName);
		
		_firstName = firstName;
		_middleName = middleName;
		_lastName = lastName;
	}
	
	public String getFullName(){
		StringBuffer sb = new StringBuffer();
		sb.append(_firstName);
		if (null != _middleName)
			sb.append(" ").append(_middleName).append(" ");
		else
			sb.append(" ");
		sb.append(_lastName);
		
		return sb.toString();
	}
}
