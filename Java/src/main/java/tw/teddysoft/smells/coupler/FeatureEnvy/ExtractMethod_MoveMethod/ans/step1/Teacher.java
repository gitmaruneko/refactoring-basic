/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.FeatureEnvy.ExtractMethod_MoveMethod.ans.step1;

import java.util.LinkedList;
import java.util.List;
import java.util.Collections;

public class Teacher {
	private String _name;
	private List<String> _specialties;

	public Teacher(String name){
		_name = name;
		_specialties = new LinkedList<>();
	}
	
	public void addSpeciality(String arg){
		_specialties.add(arg);
	}
	
	public String getName(){
		return _name;
	}
	
	public List<String> getSpecialities(){
		return Collections.unmodifiableList(_specialties);
	}
}
