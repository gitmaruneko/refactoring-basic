/*
 * Adapted from Martin Fowler's Refactoring book with modification by Teddysoft
 */
package tw.teddysoft.smells.coupler.IncompleteLibraryClass.IntroduceForeignMethod.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTest {

	@Test
	public void get_next_date() throws ParseException {

		SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		Date previousEnd = dateFormat.parse("2019-03-02 00:00:00");

		Date newStart = new Date(previousEnd.getYear(), previousEnd.getMonth(), previousEnd.getDate() + 1);

		assertEquals("Sun Mar 03 00:00:00 CST 2019", newStart.toString());
	}
}
