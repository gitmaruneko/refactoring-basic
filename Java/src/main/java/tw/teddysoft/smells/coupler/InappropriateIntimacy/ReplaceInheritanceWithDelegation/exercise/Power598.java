/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.ReplaceInheritanceWithDelegation.exercise;

public class Power598 extends Engine307{
	
	@Override
	public void setUnderMonitoring(boolean underMonitoring){
		super.setUnderMonitoring(underMonitoring);
		if (underMonitoring)
			setTestMode();
		else setNormalMode();
	}
}
