/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MiddleMan.RemoveMiddleMan.exercise;

public class Location {
	private Contact _contact;
	
	public void setContact(Contact arg){
		_contact = arg;
	}
	
	public Contact getContact(){
		return _contact;
	}

	// a lot of code here
}
