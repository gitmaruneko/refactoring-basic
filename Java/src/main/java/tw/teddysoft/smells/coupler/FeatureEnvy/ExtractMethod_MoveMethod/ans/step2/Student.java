/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.FeatureEnvy.ExtractMethod_MoveMethod.ans.step2;

public class Student {
	private String _name;
	private String _id;
	private String _department;
	
	public Student(String name, String id, String dep){
		_name = name;
		_id = id;
		_department = dep;
	}

	public String getName(){
		return _name;
	}
	
	public String getID(){
		return _id;
	}
	
	public String getDepartment(){
		return _department;
	}

	void reportStudent(StringBuffer sb) {
		sb.append("Name: ").append(getName()).append("\n");
		sb.append("ID: ").append(getID()).append("\n");
		sb.append("Department: ").append(getDepartment()).append("\n");
	}
}
