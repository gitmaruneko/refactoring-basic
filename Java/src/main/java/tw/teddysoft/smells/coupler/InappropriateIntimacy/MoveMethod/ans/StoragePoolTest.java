/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.MoveMethod.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class StoragePoolTest {

	@Test
	public void when_storage_has_different_usage_percentage_then_compress_method_implement_is_different() {
		
		Storage s1 = new Storage("ObjectCache", 5);
		StoragePool manager = new StoragePool();
		manager.addStorage(s1);

		assertEquals("fast compress", manager.compress(s1.geName()));
		s1.save(new Object());
		assertEquals("fast compress", manager.compress(s1.geName()));
		
		s1.save(new Object());
		assertEquals("standard compress", manager.compress(s1.geName()));
		s1.save(new Object());
		assertEquals("standard compress", manager.compress(s1.geName()));
	}
}
