/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.MoveMethod.ans;

import java.util.Hashtable;
import java.util.Map;

public class StoragePool {
	private Hashtable<String, Storage> pools = new Hashtable<>();
	
	public StoragePool(){
	}
	
	public void addStorage(Storage storage){
		pools.put(storage.geName(), storage);
	}
	
	public String compress(String storageName){
		return getStorage(storageName).compress();
	}
	
	public void compressAll(){
		for (Map.Entry<String, Storage> entry : pools.entrySet())
		{
			compress(entry.getKey());
		}
	}

	private Storage getStorage(String name){
		return pools.get(name);
	}
	
	// a lot of code here
}
