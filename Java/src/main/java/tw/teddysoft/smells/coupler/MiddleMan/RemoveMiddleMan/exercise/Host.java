/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MiddleMan.RemoveMiddleMan.exercise;

import java.util.Hashtable;

public class Host {
	private String _name;
	private String _address;
	private Hashtable<String, Service> _services;
	private Location _location;
	
	public Host(String name){
		_name = name;
		_services = new Hashtable<>();
	}
	
	public void setLocation(Location arg){
		_location = arg;
	}
	
	public Location getLocation(){
		return _location;
	}

	public void addService(Service arg){
		_services.put(arg.getName(), arg);
	}
	
	public Service getService(String serviceName){
		return _services.get(serviceName);
	}
	
	public String getName(){
		return _name;
	}

	public void setAddress(String arg){
		_address = arg;
	}

	public String getAddress(){
		return _address;
	}
	
	// a lot of code here
}
