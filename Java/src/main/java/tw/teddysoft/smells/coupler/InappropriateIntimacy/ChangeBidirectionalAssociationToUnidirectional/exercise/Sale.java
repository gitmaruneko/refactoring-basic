/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.ChangeBidirectionalAssociationToUnidirectional.exercise;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Sale {
	private List<SalesLineItem> _items = new LinkedList<>();
	private Date _date;
	private final String _id;
	
	public Sale(String id){
		_id = id;
	}
	
	public String getID(){
		return _id;
	}
	
	public void addLineItem(SalesLineItem lineItem){
		lineItem.setSale(this);
		_items.add(lineItem);
	}
	
	public int getNumberOfItems(){
		int result = 0;
		for(SalesLineItem item : _items){
			result += item.getQuantity();
		}
		return result;
	}
}
