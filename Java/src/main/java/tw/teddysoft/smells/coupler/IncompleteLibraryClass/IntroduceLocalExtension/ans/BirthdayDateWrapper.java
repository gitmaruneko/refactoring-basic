/*
 * Adapted from Martin Fowler's Refactoring book with modification by Teddysoft
 */
package tw.teddysoft.smells.coupler.IncompleteLibraryClass.IntroduceLocalExtension.ans;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class BirthdayDateWrapper {
	private Date _original;
	
	public BirthdayDateWrapper(){
		_original = new Date();
	}

	public BirthdayDateWrapper(Date date){
		_original = new Date(date.getTime());
	}
	
	public long getYear(){
		return _original.getTime();
	}
	
	public boolean after(Date date){
		return _original.after(date);
	}
	
	public boolean after(BirthdayDateWrapper date){
		return _original.after(date.getOriginal());
	}
	
	private Date getOriginal(){
		return _original;
	}
	
	// a lot of wrapping code here
	
	
	public Date getChiangKaiShekBirthday(){
		DateFormat df = DateFormat.getDateInstance();
		try {
			return df.parse("1887/10/31");
		} catch (ParseException e) {
			throw new RuntimeException(e.toString());
		}
	}
	
	public Date getSunYatSenBirthday(){
		DateFormat df = DateFormat.getDateInstance();
		try {
			return df.parse("1866/11/12");
		} catch (ParseException e) {
			throw new RuntimeException(e.toString());
		}
	}
	
}
