/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.ChangeBidirectionalAssociationToUnidirectional.exercise;

public class Item {
	private String _name;
	private int _price;
	
	public Item(String name, int price){
		_name = name;
		_price = price;
	}

}
