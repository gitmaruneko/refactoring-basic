/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.ReplaceInheritanceWithDelegation.ans;

public class Power598 {
	Engine307 _engine;
	
	public Power598(Engine307 engine){
		_engine = engine;
	}
	
	public void setUnderMonitoring(boolean underMonitoring){
		_engine.setUnderMonitoring(underMonitoring);
		if(underMonitoring)
			_engine.setManualControl(1000);
	}
	
	public int run(){
		return _engine.run();
	}
}
