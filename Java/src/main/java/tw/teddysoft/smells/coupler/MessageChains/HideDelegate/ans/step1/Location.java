/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MessageChains.HideDelegate.ans.step1;

public class Location {
	private String _name;
	private Contact _contact;

	public Location(String name){
		_name = name;
	}
	
	public String getName(){
		return _name;
	}
	
	public void setContact(Contact arg){
		_contact = arg;
	}
	
	public Contact getContact(){
		return _contact;
	}

	// a lot of code here
}
