/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.FeatureEnvy.ExtractMethod_MoveMethod.ans.step2;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CourseTest {
	private Teacher _teddy;
	private Student _tony;
	private Student _john;
	private Student _mary;
	
	@Before
	public void setUp(){
		_teddy = new Teacher("Teddy Chen");
		_teddy.addSpeciality("Agile Methods and Practices");
		_teddy.addSpeciality("Exception Handling");
		_teddy.addSpeciality("Pattern and Pattern Language");
		
		_tony = new Student("Tony Cheng", "S098221", "CS");
		_john = new Student("John Wu", "S099510", "SE");
		_mary = new Student("Mary Lee", "S098212", "CS");
	}
	
	
	@Test
	public void test() {
		Course refactoring = new Course("Software Refactoring");
		refactoring.setTeacher(_teddy);
		refactoring.addStudent(_tony);
		refactoring.addStudent(_john);
		refactoring.addStudent(_mary);
	}

}
