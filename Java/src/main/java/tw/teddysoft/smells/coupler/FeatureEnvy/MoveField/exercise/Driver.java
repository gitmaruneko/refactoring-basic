/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.FeatureEnvy.MoveField.exercise;

public class Driver {
	private String _name;
	private Bus _bus;
	private BusType _busType;
	
	public Driver(String name, Bus bus, BusType busType){
		_name = name;
		_bus = bus;
		_busType = busType;
	}
	
	public BusType getBusType(){
		return _busType;
	}
	
	public String getName(){
		return _name;
	}
	
	public Bus getBus(){
		return _bus;
	}
	
	// a lot of code related to the Driver class
}
