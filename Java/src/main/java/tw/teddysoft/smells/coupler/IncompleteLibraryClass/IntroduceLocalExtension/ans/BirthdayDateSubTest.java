/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.IncompleteLibraryClass.IntroduceLocalExtension.ans;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

public class BirthdayDateSubTest {

	@Test
	public void show_how_to_use_BirthdayDateSub() {
		BirthdayDateSub date = new BirthdayDateSub();
		Date cksBrithday = date.getChiangKaiShekBirthday();
		BirthdayDateSub sysBirthday = new BirthdayDateSub(date.getSunYatSenBirthday());
		
		assertEquals("Mon Oct 31 00:00:00 CST 1887", cksBrithday.toString());
		assertEquals(false, sysBirthday.after(cksBrithday));
	}
}
