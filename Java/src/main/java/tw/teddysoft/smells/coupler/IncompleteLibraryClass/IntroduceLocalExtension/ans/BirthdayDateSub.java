/*
 * Adapted from Martin Fowler's Refactoring book with modification by Teddysoft
 */
package tw.teddysoft.smells.coupler.IncompleteLibraryClass.IntroduceLocalExtension.ans;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class BirthdayDateSub extends Date {
	public BirthdayDateSub(){
		super();
	}

	public BirthdayDateSub(Date date){
		super(date.getTime());
	}
	
	public Date getChiangKaiShekBirthday(){
		DateFormat df = DateFormat.getDateInstance();
		try {
			return df.parse("1887/10/31");
		} catch (ParseException e) {
			throw new RuntimeException(e.toString());
		}
	}
	
	public Date getSunYatSenBirthday(){
		DateFormat df = DateFormat.getDateInstance();
		try {
			return df.parse("1866/11/12");
		} catch (ParseException e) {
			throw new RuntimeException(e.toString());
		}
	}
	
}
