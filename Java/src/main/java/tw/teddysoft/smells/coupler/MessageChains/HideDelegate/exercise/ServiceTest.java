/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MessageChains.HideDelegate.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class ServiceTest {

	@Test
	public void when_config_Service_correctly_then_can_get_host_and_contact_name() {
		Location office = new Location("Office");
		Contact teddy = new Contact("Teddy", "02-2311-6230");
		office.setContact(teddy);

		Host teddyWin7 = new Host("TeddyWin7_x64");
		teddyWin7.setLocation(office);
		
		Service checkDiskSpace = new Service("checkDiskSpace");
		checkDiskSpace.setHost(teddyWin7);
	
		assertEquals("TeddyWin7_x64", checkDiskSpace.getHost().getName());
		assertEquals("Teddy", checkDiskSpace.getHost().getLocation().getContact().getName());
	}
}
