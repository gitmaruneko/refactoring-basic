/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.IncompleteLibraryClass.IntroduceLocalExtension.ans;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

public class BirthdayDateWrapperTest {

	@Test
	public void show_how_to_use_BirthdayDateWrapper() {

		BirthdayDateWrapper date = new BirthdayDateWrapper();
		Date cksBirthday = date.getChiangKaiShekBirthday();
		BirthdayDateWrapper sysBirthday = new BirthdayDateWrapper(date.getSunYatSenBirthday());
		
		assertEquals("Mon Oct 31 00:00:00 CST 1887", cksBirthday.toString());
		assertEquals(false, sysBirthday.after(cksBirthday));
	}
}
