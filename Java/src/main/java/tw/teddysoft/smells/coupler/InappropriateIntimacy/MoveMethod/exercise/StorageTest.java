/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.MoveMethod.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

import tw.teddysoft.smells.coupler.InappropriateIntimacy.MoveMethod.ans.Storage;

public class StorageTest {

	@Test
	public void testUsagePercentageIsCorrect() {
		Storage storage = new Storage("Defaule", 10);
		assertEquals(0, storage.getUsagePercentage());
		
		addObjects(3, storage, new Object());
		assertEquals(30, storage.getUsagePercentage());
		
		addObjects(7, storage, new Object());
		assertEquals(100, storage.getUsagePercentage());
	}
	
	@Test
	public void when_storage_has_different_usage_percentage_then_save_method_implement_is_different() {
		Storage storage = new Storage("Defaule", 5);
		assertEquals("fast save", storage.save(new Object()));
		assertEquals("fast save", storage.save(new Object()));
		assertEquals("standard save", storage.save(new Object()));
		assertEquals("standard save", storage.save(new Object()));
		assertEquals("standard save", storage.save(new Object()));
	}
	
	private void addObjects(int number, Storage storage, Object obj){
		for(int i = 0; i < number; i++)
			storage.save(obj);
	}
}
