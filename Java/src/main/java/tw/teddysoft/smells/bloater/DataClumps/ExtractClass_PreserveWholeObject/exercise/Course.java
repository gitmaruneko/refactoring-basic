/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.DataClumps.ExtractClass_PreserveWholeObject.exercise;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Course {

	private String _dbDriverName;
	private String _dbURL;
	private String _dbUserName;
	private String _dbPassword;
	
	public void setDBDriveName(String arg){
		_dbDriverName = arg;
	}

	public void setDBURL(String arg){
		_dbURL = arg;
	}
	
	public void setDBUserName(String arg){
		_dbUserName = arg;
	}
	
	public void setDBPassword(String arg){
		_dbPassword = arg;
	}
	
	// a lot of code
	
	public boolean save(){
		Connection conn = null;
		
		try
		{
		  Class.forName(_dbDriverName).newInstance();
		  String url = _dbURL;
		  conn = DriverManager.getConnection(url, _dbUserName, _dbPassword);
		  // .. save this to database
		}
		catch (Exception e) {
			System.out.println(e);
			return false;
		}
		finally{
			try {
				if(null != conn)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
}


