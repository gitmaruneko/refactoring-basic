/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LongParameterList.PreserveWholeObject.exercise;

public class Person {

	private String _name;
	private String _address;
	private String _phone;
	
	public Person(String name, String address, String phone){
		_name = name;
		_address = address;
		_phone = phone;
	}
	
	public String getName(){
		return _name;
	}
	
	public String getAddress(){
		return _address;
	}
	
	public String getPhone(){
		return _phone;
	}
	
}
