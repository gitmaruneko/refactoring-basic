/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractClass.ans;

public class USB3Controller extends USBController {
	private static final int SPEED_MEGA_BIT = 5 * 1000;
	private static final int USB_3_VERSION = 30;
	
	public USB3Controller(String manufacturer){
		super(USB_3_VERSION, manufacturer);
	}

	@Override
	public int getChannelSpeed() {
		return SPEED_MEGA_BIT;
	}
}
