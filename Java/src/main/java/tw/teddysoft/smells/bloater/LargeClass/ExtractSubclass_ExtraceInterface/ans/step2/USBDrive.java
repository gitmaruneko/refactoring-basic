/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractSubclass_ExtraceInterface.ans.step2;

public interface USBDrive {
	public int getSpeedInMegaBit();	
	public boolean connect();
	public String checkDisk();
}