/*
 * Adapted from Martin Fowler's Refactoring Book
 * 
 */
package tw.teddysoft.smells.bloater.LongMethod.ReplaceTempWithQuery.exercise;

public class Product {
	private int _itemPrice;
	private int _quantity;

	public Product(int itemPrice, int quantity){
		_itemPrice = itemPrice;
		_quantity = quantity;
	}
	
	public double getPrice(){
		double basePrice = _quantity * _itemPrice;
		if(basePrice > 1000)
			return basePrice * 0.95;
		else
			return basePrice * 0.98;
	}
}
