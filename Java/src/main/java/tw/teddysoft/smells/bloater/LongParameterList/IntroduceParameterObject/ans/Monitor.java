/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LongParameterList.IntroduceParameterObject.ans;

import java.util.Hashtable;

public class Monitor {
	private Hashtable<String, Device> _devices = new Hashtable<>();
	
	public Monitor(){
	}
	
	public void addDevice(Device device){
		_devices.put(device.getName(), device);
	}

	public Hashtable<String, Device> getDevicesReadingBetween(Range range){
		Hashtable<String, Device> result = new Hashtable<>();
		
		for(Device device : _devices.values()){
			if(range.isInclude(device.getReading()))
				result.put(device.getName(), device);	
		}
		return result;
	}
	
//	public Hashtable<String, Device> getDevicesReadingBetween(Range range){
//		Hashtable<String, Device> result = new Hashtable<>();
//		
//		for(Device device : _devices.values()){
//			if ( (device.getReading() <= range.getHigh()) &&
//				(device.getReading() >= range.getLow())){
//				result.put(device.getName(), device);	
//			}
//		}
//		return result;
//	}
}
