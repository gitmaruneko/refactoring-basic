/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LongParameterList.PreserveWholeObject.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class CatTest {

	@Test
	public void when_I_call_getDetailInfo_with_right_data_then_I_got_the_right_result() {
		Cat eiffel = new Cat("Eiffel");
		StringBuilder sb = new StringBuilder();
		sb.append("Cat Name: ").append("Eiffel").append("\n");
		sb.append("Master Name: ").append("Teddy").append("\n");
		sb.append("Master Address: ").append("Old Taipei City").append("\n");
		sb.append("Master masterPhone: ").append("02-2311-6230").append("\n");
		
		Person teddy = new Person("Teddy", "Old Taipei City", "02-2311-6230");
		
		assertEquals(sb.toString(), eiffel.getDetailInfo(teddy.getName(), teddy.getAddress(), teddy.getPhone()));
	}
}
