/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractClass.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class USBDriveTest {

	@Test
	public void when_create_USBDrive_with_version_20_and_a_null_controller_then_get_USB2_speed_and_version() {
		USBDrive drive = new USBDrive(20, null);
		assertEquals(480, drive.getSpeedInMegaBit());
		assertEquals(20, drive.getVersion());
	}

	@Test
	public void when_create_USBDrive_with_version_20_and_a_USB3Controller_then_get_USB2_speed_and_version() {
		USBDrive drive = new USBDrive(20, new USB3Controller("Intel"));
		assertEquals(480, drive.getSpeedInMegaBit());
		assertEquals(20, drive.getVersion());
	}

	@Test
	public void when_create_USBDrive_with_version_30_and_a_USB3Controller_then_get_USB3_speed_and_version() {
		USBDrive drive = new USBDrive(30, new USB3Controller("Intel"));
		assertEquals(5000, drive.getSpeedInMegaBit());
		assertEquals(30, drive.getVersion());
	}

	@Test
	public void when_create_USBDrive_with_version_30_and_a_null_USB3Controller_then_raise_RuntimeException() {
		USBDrive drive = new USBDrive(30, null);
		try{
			assertEquals(5000, drive.getSpeedInMegaBit());
			fail();
		}
		catch(RuntimeException e){
			assertTrue(true);
		}
	}

}
