/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.DataClumps.ExtractClass_PreserveWholeObject.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class DataClumpsTest {

	@Test
	public void demo_a_client_of_Course() {
		Course course = new Course();
		course.setDBDriveName("org.apache.derby.jdbc.ClientDriver");
		course.setDBURL("jdbc:derby:testdb;create=true");
		course.setDBUserName("teddy");
		course.setDBPassword("ao1xx");
		// a lot of code
		assertTrue(course.save());
	}

	@Test
	public void demo_a_client_of_Teacher() {
		Teacher teacher = new Teacher("org.apache.derby.jdbc.ClientDriver", "jdbc:derby:testdb;create=true", "teddy", "ao1xx");
		// a lot of code
		assertTrue(teacher.save());
	}
	
	@Test
	public void demo_a_client_of_Student() {
		Student student = new Student();
		// a lot of code
		assertTrue(student.save("org.apache.derby.jdbc.ClientDriver", "jdbc:derby:testdb;create=true", "teddy", "ao1xx"));
	}
}
