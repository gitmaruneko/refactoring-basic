/*
 * Adapted from Martin Fowler's Refactoring Book
 * 
 */
package tw.teddysoft.smells.bloater.LongMethod.ReplaceMethodWithMethodObject.ans;

public class Gamma {
	private final Account _account;
	private int inputVal;
	private int quantity;
	private int yearToDate;
	
	public Gamma (Account account, int inputVal, int quantity, int yearToDate){
		this._account = account;
		this.inputVal = inputVal;
		this.quantity = quantity;
		this.yearToDate = yearToDate;
	}
	
	public int compute(){
		int importantValue1 = (inputVal * quantity) + _account.delta();
		int importantValue2 = (inputVal + yearToDate) + 100;
		if ((yearToDate - importantValue1) > 100)
			importantValue2 -= 20;
		int importantValue3 = importantValue2 * 7;
		
		// an so on...
		
		return importantValue3 - 2 * importantValue1;
	}
}
