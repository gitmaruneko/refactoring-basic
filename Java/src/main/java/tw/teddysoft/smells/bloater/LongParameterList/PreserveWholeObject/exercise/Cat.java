/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LongParameterList.PreserveWholeObject.exercise;

public class Cat {

	private String _name;
	
	//a lot of code .....
	
	public Cat(String name){
		_name = name;
	}
	
	public String getDetailInfo(String masterName, String masterAddress, String masterPhone){
		StringBuilder sb = new StringBuilder();

		sb.append("Cat Name: ").append(_name).append("\n");
		sb.append("Master Name: ").append(masterName).append("\n");
		sb.append("Master Address: ").append(masterAddress).append("\n");
		sb.append("Master masterPhone: ").append(masterPhone).append("\n");
		
		return sb.toString();
	}
	
	
}
