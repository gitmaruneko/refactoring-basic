/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithStateStrategy.ans.step3;

public class USB extends HardDriveType {
	@Override
	public int getTypeCode(){
		return HardDrive.USB;
	}
}
