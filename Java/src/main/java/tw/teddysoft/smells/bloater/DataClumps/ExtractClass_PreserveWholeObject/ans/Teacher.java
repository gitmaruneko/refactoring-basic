/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.DataClumps.ExtractClass_PreserveWholeObject.ans;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Teacher {

	ConnectionData _connData;
	
	public Teacher(ConnectionData arg){
		_connData = arg;
	}
	
	// a lot of code
	
	public boolean save(){
		Connection conn = null;
		
		try
		{
		  Class.forName(_connData.getDriverName()).newInstance();
		  conn = DriverManager.getConnection(_connData.getURL(), _connData.getUserName(), _connData.getPassword());
		  // .. save this to database
		}
		catch (Exception e) {
			System.out.println(e);
			return false;
		}
		finally{
			try {
				if(null != conn)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
}
