/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithClass.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonTest {

	@Test
	public void when_setBloodGroup_out_of_range_then_nothing_happened() {
		Person teddy = new Person(Person.O);
		assertEquals(0, teddy.getBooldGroup());

		teddy.setBloodGroup(5);
		assertEquals(5, teddy.getBooldGroup());
	}
}
