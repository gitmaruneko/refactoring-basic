/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithSubclasses.ans;

import static org.junit.Assert.*;
import org.junit.Test;


public class HardDriveTest {
	@Test
	public void demo_a_client_of_HardDrive() {
		HardDrive sata = HardDrive.create(HardDrive.SATA);
		HardDrive sas =  HardDrive.create(HardDrive.SAS);
		HardDrive scsi = HardDrive.create(HardDrive.SCSI);
		HardDrive usb = HardDrive.create(HardDrive.USB);
		
		assertEquals(HardDrive.SATA, sata.getType());
		assertEquals(HardDrive.SAS, sas.getType());
		assertEquals(HardDrive.SCSI, scsi.getType());
		assertEquals(HardDrive.USB, usb.getType());
		
		assertTrue(sata.smartCheck());
		assertTrue(sas.smartCheck());
		assertTrue(scsi.smartCheck());
		assertFalse(usb.smartCheck());
	}
}
