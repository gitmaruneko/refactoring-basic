/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractSubclass_ExtraceInterface.ans.step1;

public class USB3Controller {
	private String _manufacturer;
	
	public static USB3Controller newDefaultController(){
		return new USB3Controller("Teddysoft");
	}
	
	public USB3Controller(String manufacturer){
		_manufacturer = manufacturer;
	}
	
	public int getChannelSpeed() {
		return 5;
	}
	
	public String getManufacturer(){
		return _manufacturer;
	}

}
