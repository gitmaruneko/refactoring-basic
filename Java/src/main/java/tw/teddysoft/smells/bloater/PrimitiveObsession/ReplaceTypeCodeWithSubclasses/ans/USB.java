/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithSubclasses.ans;

public class USB extends HardDrive {

	public int getType(){
		return HardDrive.USB;
	}

	@Override
	public boolean smartCheck() {
		return doUSBSmartCheck();
	}
	
	private boolean doUSBSmartCheck() {
		// a lot of code
		return false;
	}
}
