/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractClass.ans;

public class NullController extends USBController{
	private static final int SPEED_MEGA_BIT = 480;
	private static final int USB_2_VERSION = 20;
	
	public NullController(String manufacturer) {
		super(USB_2_VERSION, manufacturer);
	}

	@Override
	public int getChannelSpeed() {
		return SPEED_MEGA_BIT;
	}
}