/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LongMethod.ReplaceTempWithQuery.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProductTest {

	@Test
	public void when_price_less_then_1000_then_discount_is_98_percent() {
		int price = 50;
		int quantity = 20;
		Product product = new Product(price, quantity);
		assertEquals(price * quantity * 0.98, product.getPrice(), 0.00001);
	}

	@Test
	public void when_price_greater_then_1000_then_discount_is_95_percent() {
		int price = 1001;
		int quantity = 1;
		Product product = new Product(price, quantity);
		assertEquals(price * quantity * 0.95, product.getPrice(), 0.00001);
	}

}
