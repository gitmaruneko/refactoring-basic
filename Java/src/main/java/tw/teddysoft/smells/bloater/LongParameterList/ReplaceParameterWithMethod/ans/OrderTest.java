/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LongParameterList.ReplaceParameterWithMethod.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class OrderTest {

	@Test
	public void when_quantity_greater_than_100_then_discount_90percent() {
		Order order = new Order(101, 50);
		assertEquals(505, order.getPrice(), 0.00001);
	}

	@Test
	public void when_quantity_less_than_100_then_discount_50percent() {
		Order order = new Order(10, 50);
		assertEquals(250, order.getPrice(), 0.00001);
	}
}
