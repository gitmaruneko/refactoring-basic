/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LongParameterList.IntroduceParameterObject.ans;

public class Device {

	private String _name;
	private double _reading;
	
	//a lot of code .....
	
	public Device(String name, double reading){
		_name = name;
		_reading = reading;
	}
	
	public String getName(){
		return _name;
	}
	
	public double getReading(){
		return _reading;
	}
	
}
