/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractClass.ans;

public abstract class USBController {
	private int _version;
	private String _manufacturer;
	
	static USBController newNull(){
		return new NullController("FakeSoftware");
	}
	
	public USBController(int version, String manufacturer) {
		_version = version;
		_manufacturer = manufacturer;
	}

	public abstract int getChannelSpeed();

	public String getManufacturer() {
		return _manufacturer;
	}
	
	public int getVersion(){
		return _version;
	}
}