/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LongMethod.ReplaceMethodWithMethodObject.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class AccountTest {

	@Test
	public void when_invoke_gamma_with_100_2_6_then_I_got_1038() {
		Account teddy = new Account();
		assertEquals(1038, teddy.gamma(100, 2, 6));
	}
}
