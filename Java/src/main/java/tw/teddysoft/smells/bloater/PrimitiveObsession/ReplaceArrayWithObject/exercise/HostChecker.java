/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceArrayWithObject.exercise;

public class HostChecker {

	private Command _cmd;
	
	public HostChecker(Command  cmd){
		_cmd = cmd;
	}

	public String [] execute(){
		return _cmd.run();
	}
	
}
