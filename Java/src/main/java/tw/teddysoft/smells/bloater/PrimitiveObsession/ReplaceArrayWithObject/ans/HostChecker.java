/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceArrayWithObject.ans;

public class HostChecker {
	private Command _cmd;
	
	public HostChecker(Command  cmd){
		_cmd = cmd;
	}

	public CheckResult execute(){
		return _cmd.run();
	}
}
