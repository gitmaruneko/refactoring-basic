/*
 * Adapted from Martin Fowler's Refactoring Book by Teddysoft
 * 
 */
package tw.teddysoft.smells.bloater.LongParameterList.ReplaceParameterWithMethod.ans;

public class Order {
	private int _quantity;
	private int _itemPrice;
	
	public Order(int quantity, int itemPrice){
		_quantity = quantity;
		_itemPrice = itemPrice;
	}
	
	public double getPrice(){
		return discountedPrice();
	}

	private int getBasePrice() {
		int basePrice = _quantity * _itemPrice;
		return basePrice;
	}

	private int getDiscountLevel() {
		int discountLevel;
		if(_quantity > 100){
			discountLevel = 2;
		}
		else{
			discountLevel = 1;
		}
		return discountLevel;
	}
	
	private double discountedPrice(){
		if (getDiscountLevel() == 2){
			return getBasePrice() * 0.1;
		}
		else{
			return getBasePrice() * 0.5;
		}
	}
	
	public class OrderV2 {
		
		// apply Inline Method 
		public double getPrice(){
			if (getDiscountLevel() == 2){
				return getBasePrice() * 0.1;
			}
			else{
				return getBasePrice() * 0.5;
			}
		}
	}
}
