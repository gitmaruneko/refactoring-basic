/*
 * Adapted from Martin Fowler's Refactoring Book
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceDataValueWithObject.ans;

public class Customer {
	private final String _customer;
	
	public Customer(String name){
		_customer = name;
	}
	
	public String getName(){
		return _customer;
	}
}
