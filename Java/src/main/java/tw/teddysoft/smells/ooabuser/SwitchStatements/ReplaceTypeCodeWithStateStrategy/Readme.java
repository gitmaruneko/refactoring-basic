/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceTypeCodeWithStateStrategy;

import tw.teddysoft.common.annotation.See;
import tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithSubclasses.exercise.HardDrive;
import tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithSubclasses.exercise.HardDriveTest;

@See({HardDriveTest.class, HardDrive.class})
public interface Readme {
}
