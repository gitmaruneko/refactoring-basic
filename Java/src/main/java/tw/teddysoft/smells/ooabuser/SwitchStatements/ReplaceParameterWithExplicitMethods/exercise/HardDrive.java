/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceParameterWithExplicitMethods.exercise;

public abstract class HardDrive {

	static final int SATA = 0;
	static final int SAS = 1;
	static final int SCSI = 2;
	static final int USB = 3;
	
	protected HardDrive(){}

	public static HardDrive create(int type){
		switch(type){
		case SATA:
			return new SATA();
		case SAS:
			return new SAS();
		case SCSI:
			return new SCSI();
		case USB:
			return new USB();
		default:
			throw new RuntimeException("Unsupported hard drive type: " + type);
		}
	}

}
