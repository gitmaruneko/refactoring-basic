package tw.teddysoft.smells.ooabuser.AlternativeClassesWithDifferentInterface.RemoveMethod;

import tw.teddysoft.common.annotation.See;
import tw.teddysoft.smells.ooabuser.AlternativeClassesWithDifferentInterface.RenameMethod.exercise.DemoSmellClientTest;
import tw.teddysoft.smells.ooabuser.AlternativeClassesWithDifferentInterface.RenameMethod.exercise.Logger;
import tw.teddysoft.smells.ooabuser.AlternativeClassesWithDifferentInterface.RenameMethod.exercise.Log;

@See({Log.class, Logger.class, DemoSmellClientTest.class})
public interface Readme {
}
