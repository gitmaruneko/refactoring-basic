/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.exercise;

public abstract class HardDriveType {
	static final int SATA = 0;
	static final int SAS = 1;
	static final int SCSI = 2;
	static final int USB = 3;
	
	public static HardDriveType newType(int code){
		switch(code){
		case SATA:
			return new SATA();
		case SAS:
			return new SAS();
		case SCSI:
			return new SCSI();
		case USB:
			return new USB();
		default:
			throw new IllegalArgumentException("Incorrent hard dirve type code");
		}
	}
	
	public abstract int getTypeCode();
}
