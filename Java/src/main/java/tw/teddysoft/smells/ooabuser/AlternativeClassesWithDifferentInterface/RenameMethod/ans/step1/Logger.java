package tw.teddysoft.smells.ooabuser.AlternativeClassesWithDifferentInterface.RenameMethod.ans.step1;

public class Logger {
	public static Logger makeLogger(String fileName){
		return new Logger();
	}
	protected Logger(){};
	public void informational(String msg){
		//...
	}
	public void informational(String msg, Exception e){
		//...
	}
	public void warning(String msg){
		//...
	}
	public void warning(String msg, Exception e){
		//...
	}
	public void error(String msg){
		//...
	}
	public void error(String msg, Exception e){
		//...
	}
	public void fatal(String msg){
		//...
	}
	public void fatal(String msg, Exception e){
		//...
	}

}
