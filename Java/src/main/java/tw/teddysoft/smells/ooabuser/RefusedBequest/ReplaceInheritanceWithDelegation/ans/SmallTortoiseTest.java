/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.RefusedBequest.ReplaceInheritanceWithDelegation.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class SmallTortoiseTest {

	@Test
	public void when_I_Replace_Inheritance_With_Delegation_then_SmallTortoise_has_a_clearer_interface() {
		AccessPoint ap = new AccessPoint();
		assertTrue(ap.wirelessConnect());
	
		SmallTortoise st = new SmallTortoise(ap);
		assertTrue(st.wireConnect());
	}
}
