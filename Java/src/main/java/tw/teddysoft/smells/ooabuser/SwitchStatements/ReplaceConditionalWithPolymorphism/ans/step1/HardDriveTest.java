/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.ans.step1;

import static org.junit.Assert.*;

import org.junit.Test;

public class HardDriveTest {

	@Test
	public void demo_a_client_of_HardDrive() {
		HardDrive sata = new HardDrive(HardDriveType.SATA);
		HardDrive sas = new HardDrive(HardDriveType.SAS);
		HardDrive scsi = new HardDrive(HardDriveType.SCSI);
		HardDrive usb = new HardDrive(HardDriveType.USB);
		
		assertEquals(HardDriveType.SATA, sata.getType());
		assertEquals(HardDriveType.SAS, sas.getType());
		assertEquals(HardDriveType.SCSI, scsi.getType());
		assertEquals(HardDriveType.USB, usb.getType());
		
		assertTrue(sata.smartCheck());
		assertTrue(sas.smartCheck());
		assertTrue(scsi.smartCheck());
		assertFalse(usb.smartCheck());
	}
}
