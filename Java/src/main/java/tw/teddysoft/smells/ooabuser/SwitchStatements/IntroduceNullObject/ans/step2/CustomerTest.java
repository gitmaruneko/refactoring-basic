/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.IntroduceNullObject.ans.step2;

import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {

	@Test
	public void when_site_with_null_customer_then_get_default_value_0() {
		Site site = new Site();
		assertEquals(0, getWeeksDelinquentInLastYear(site));
	}
	
	@Test
	public void when_customer_with_valid_PaymentHistory_then_get_normal_result() {
		Customer customer = new Customer("Teddy");
		customer.setHistory(new PaymentHistory());
		
		Site site = new Site(customer);
		assertEquals(2, getWeeksDelinquentInLastYear(site));
	}
	
	@Test
	public void when_customer_with_null_PaymentHistory_then_raise_NullPointerException() {
		Site site = new Site(new Customer("Teddy"));
		try{
			assertEquals(0, getWeeksDelinquentInLastYear(site));
			fail();
		}
		catch(NullPointerException e){
			assertTrue(true);
		}
	}
	
	/*
	 * Simulate the client of the Site class
	 * The if condition was removed by applying Introduce Null Object refactoring
	 */
	private int getWeeksDelinquentInLastYear(Site site){
		Customer customer = site.getCurstomer();
		return customer.getHistory().getWeeksDelinquentInLastYear();
	}
}
