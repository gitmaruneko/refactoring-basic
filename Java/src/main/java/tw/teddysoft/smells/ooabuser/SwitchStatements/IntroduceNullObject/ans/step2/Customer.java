/*
 * Adapted from Martin Fowler's Refactoring Book 
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.IntroduceNullObject.ans.step2;

public class Customer implements Nullable {
	private String _name;
	private PaymentHistory history;

	public static Customer newNull(){
		return new NullCustomer("");
	}
	
	public Customer(String name){
		_name = name;
	}
	
	public String getName(){
		return _name;
	}

	public void setHistory(PaymentHistory arg){
		history = arg;
	}
	
	public PaymentHistory getHistory(){
		return history;
	}

	@Override
	public boolean isNull() {
		return false;
	}
}
