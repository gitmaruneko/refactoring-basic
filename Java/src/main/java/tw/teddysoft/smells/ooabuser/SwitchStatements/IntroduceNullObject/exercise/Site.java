/*
 * Adapted from Martin Fowler's Refactoring Book 
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.IntroduceNullObject.exercise;

public class Site {
	private Customer _customer;

	public Site(){
		this(null);
	}
	
	public Site(Customer customer){
		_customer = customer;
	}
	
	public Customer getCurstomer(){
		return _customer;
	}
}
