/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceParameterWithExplicitMethods.ans;

public abstract class HardDrive {

	static final int SATA = 0;
	static final int SAS = 1;
	static final int SCSI = 2;
	static final int USB = 3;
	
	protected HardDrive(){}
	
	public static HardDrive createSATA(){
		return new SATA();
	}

	public static HardDrive createSAS(){
		return new SAS();
	}
	
	public static HardDrive createSCSI(){
		return new SCSI();
	}
	
	public static HardDrive createUSB(){
		return new USB();
	}
	
}
