/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.ans.stepX;

public class SaveMostSpaceFirst implements RepairStrategy {
	@Override
	public int perform() {
		int repairedByte = 8000;
		// a log of code
		return repairedByte;
	}
}
