/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.RefusedBequest.ReplaceInheritanceWithDelegation.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class SmallTortoiseTest {

	@Test
	public void demo_refused_bequest_violate_Liskov_substitution_principle() {
		AccessPoint ap = new AccessPoint();
		assertTrue(ap.wirelessConnect());
	
		try{
			ap = new SmallTortoise();
			assertTrue(ap.wirelessConnect());
			fail();
		}catch(RuntimeException e){
			assertTrue(true);
		}
	}
}
