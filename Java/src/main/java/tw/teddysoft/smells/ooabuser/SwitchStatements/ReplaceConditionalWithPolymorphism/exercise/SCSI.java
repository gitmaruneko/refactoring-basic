/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.exercise;

public class SCSI extends HardDriveType {
	@Override
	public int getTypeCode(){
		return SCSI;
	}
}
