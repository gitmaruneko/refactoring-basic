/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceParameterWithExplicitMethods.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class HardDriveTest {
	
	@Test
	public void demo_using_factory_method_to_create_different_types_of_objects(){
		HardDrive sata = HardDrive.create(HardDrive.SATA);
		HardDrive sas =  HardDrive.create(HardDrive.SAS);
		HardDrive scsi = HardDrive.create(HardDrive.SCSI);
		HardDrive usb = HardDrive.create(HardDrive.USB);

		assertNotNull(sata);
		assertNotNull(sas);
		assertNotNull(scsi);
		assertNotNull(usb);
		
		assertTrue(sata instanceof SATA);
		assertTrue(sas instanceof SAS);
		assertTrue(scsi instanceof SCSI);
		assertTrue(usb instanceof USB);
	}
}
