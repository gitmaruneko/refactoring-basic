/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.ans.stepX;

public class SAS extends HardDrive {

	public int getType(){
		return HardDrive.SAS;
	}

	@Override
	public boolean smartCheck() {
		return doSASSmartCheck();
	}

	private boolean doSASSmartCheck() {
		return true;
	}
}
