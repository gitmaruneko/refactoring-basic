/*
 * adapted from William C. Wake's Refactoring Workbook
 */
package tw.teddysoft.smells.ooabuser.AlternativeClassesWithDifferentInterface.RenameMethod.ans.step3;

import java.io.File;

public final class Log {
	public static int INFO=1, WARN=2, ERROR=3, FATAL=4;
	
	public static void setLog(File f){
		// .... 
	}
	
	public static void log(int level, String msg){
		// ...
	}
	
	
	public static void log(int level, String msg, Exception e){
		// ...
	}
}
