/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.ans.step1;

public abstract class HardDriveType {
	static final int SATA = 0;
	static final int SAS = 1;
	static final int SCSI = 2;
	static final int USB = 3;
	
	public static HardDriveType newType(int code){
		switch(code){
		case SATA:
			return new SATA();
		case SAS:
			return new SAS();
		case SCSI:
			return new SCSI();
		case USB:
			return new USB();
		default:
			throw new IllegalArgumentException("Incorrent hard dirve type code");
		}
	}
	
	public boolean smartCheck(HardDrive hd){
		boolean result = false;
		
		switch(getTypeCode()){
		case HardDriveType.SATA:
			result = hd.doSATASmartCheck();
			break;
		case HardDriveType.SAS:
			result = hd.doSASSmartCheck();
			break;
		case HardDriveType.SCSI:
			result = hd.doSCSISmartCheck();
			break;
		case HardDriveType.USB:
			result = hd.doUSBSmartCheck();
			break;
		default:
			throw new RuntimeException("Incorrect hard drive");
		}
		return result;
	}
	
	public abstract int getTypeCode();
}
