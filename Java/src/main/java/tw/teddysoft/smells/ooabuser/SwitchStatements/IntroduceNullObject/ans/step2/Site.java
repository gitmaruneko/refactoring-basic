/*
 * Adapted from Martin Fowler's Refactoring Book 
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.IntroduceNullObject.ans.step2;

public class Site {
	private Customer _customer;

	public Site(){
		this(null);
	}
	
	public Site(Customer customer){
		_customer = customer;
	}
	
	public Customer getCurstomer(){
		return (null == _customer) ? Customer.newNull() : _customer;
	}
}
