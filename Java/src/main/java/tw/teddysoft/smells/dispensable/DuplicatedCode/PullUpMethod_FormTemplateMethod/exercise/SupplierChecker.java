/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.PullUpMethod_FormTemplateMethod.exercise;

public class SupplierChecker extends FinancialChecker{

	@Override
	public boolean check() {
        return checkBank() 
        		&& checkCredit()
        		&& checkLoan();
	}
	
	private boolean checkBank() {
		return (_bankBalance >= 10000000) ? true : false;
	}

	private boolean checkCredit() {
		return _creditStatus;
	}

	private boolean checkLoan() {
		return _loanStatus;
	}
	
	public SupplierChecker(
			int bankBalance, 
			boolean creditStatus, 
			boolean loanStatus){
		_bankBalance = bankBalance;
		_creditStatus = creditStatus;
		_loanStatus = loanStatus;
	}
	
	private int _bankBalance;
	private boolean _creditStatus; 
	private boolean _loanStatus;

}
