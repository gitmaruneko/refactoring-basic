/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DataClass.EncapsulateCollection.ans.step2;

import static org.junit.Assert.*;
import org.junit.Test;

public class PersonTest {
	
	@Test
	public void when_I_apply_encapsulate_collection_to_Person_then_it_has_suitable_responsibilities() {
		Person teddy = new Person();
		
		teddy.addCourse(new Course("Java Programming", false));
		teddy.addCourse(new Course("Exception Handling Design", true));
		assertEquals(2, teddy.numberOfCourses());
		
		Course refactoring = new Course("Refactoring", true);
		teddy.addCourse(refactoring);
		teddy.addCourse(new Course("Scrum in practice", false));
		assertEquals(4, teddy.numberOfCourses());
	
		teddy.removeCourse(refactoring);
		assertEquals(3, teddy.numberOfCourses());
		
		assertEquals(1, teddy.numberOfAdvancedCourse());
	}

	@Test
	//public void cannot_add_a_course_to_the_collection_returned_by_getCourses
	//testGetCourseReturnReadOnlyCollection
	public void when_I_add_a_course_to_the_collection_return_by_getCourses_then_UnsupportedOperationException_is_raised() {
		Person teddy = new Person();
		Course refactoring = new Course("Refactoring", true);
		try{
			teddy.getCourses().add(refactoring);
			fail();
		}
		catch(UnsupportedOperationException e){
			assertEquals(0, teddy.numberOfCourses());
		}
	}
		
	@Test
	public void when_I_remove_a_course_to_the_collection_return_by_getCourses_then_UnsupportedOperationException_is_raised() {
		Person teddy = new Person();
		Course refactoring = new Course("Refactorng", true);
		teddy.addCourse(refactoring);
		assertEquals(1, teddy.getCourses().size());
		
		try{
			teddy.getCourses().remove(refactoring);
			fail();
		}
		catch(UnsupportedOperationException e){
			assertEquals(1, teddy.numberOfCourses());
		}
	}
	
}
