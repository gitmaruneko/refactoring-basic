/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.Comments.ExtractMethod.ans;

public class ThermalController {
	private double _workingTemperatureReading;
	private boolean _isTurnOn;
	
	public ThermalController(double workingTemperatureReading, boolean isTurnOn){
		_workingTemperatureReading = workingTemperatureReading;
		_isTurnOn = isTurnOn;
	}

	public boolean speedUp(){
		if (isNotOverheat()){
			sendCommand("0x5518");
			return true;
		}
		else
			return false;
	}	
	
	private boolean isNotOverheat(){
		return (_isTurnOn && (_workingTemperatureReading * 0.998) < 55);
	}
	
	private void sendCommand(String cmd){
		// ...
	}
}
