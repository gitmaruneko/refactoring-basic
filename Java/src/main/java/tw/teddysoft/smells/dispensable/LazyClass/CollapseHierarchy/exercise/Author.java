/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.LazyClass.CollapseHierarchy.exercise;

import java.util.LinkedList;
import java.util.List;

public class Author {
	private String _firstName;
	private String _lastName;
	private List<Publication> publications; 
	
	public Author(String firstName, String lastName){
		_firstName = firstName;
		_lastName = lastName;
		publications = new LinkedList<>();
	}
	
	public int getPublicationNumber(){
		return publications.size();
	}
	
	public String getFirstName(){
		return _firstName;
	}
	
	public String getLastName(){
		return _lastName;
	}
}
