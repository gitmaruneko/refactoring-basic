/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
//TODO refactor this test case to remove duplicated code

package tw.teddysoft.smells.dispensable.DuplicatedCode.PullUpMethod_FormTemplateMethod.ans.step1;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoanCheckerTest {
	
	private int _bankbalance = 300000;
	private boolean _creditStatus = true; 
	private boolean _loanStatus = true;
	private int _income = 22000;
	private boolean _stockStatus = true;
	
	@Test
	public void loanCheckPass() {
		LoanChecker checker = new LoanChecker(
				_bankbalance,
				_creditStatus,
				_loanStatus,
				_income,
				_stockStatus);
		assertTrue(checker.check());
	}

	@Test
	public void when_bank_balance_less_than_300000_then_check_fail() {
		_bankbalance = 299999;
		LoanChecker checker = new LoanChecker(
				_bankbalance,
				_creditStatus,
				_loanStatus,
				_income,
				_stockStatus);
		assertFalse(checker.check());
	}
	
	@Test
	public void when_income_less_than_22K_then_check_fail() {
		_income = 21999;
		LoanChecker checker = new LoanChecker(
				_bankbalance,
				_creditStatus,
				_loanStatus,
				_income,
				_stockStatus);
		assertFalse(checker.check());
	}
	
	@Test
	public void when_credit_status_fail_then_check_fail() {
		_creditStatus = false;
		LoanChecker checker = new LoanChecker(
				_bankbalance,
				_creditStatus,
				_loanStatus,
				_income,
				_stockStatus);
		assertFalse(checker.check());
	}
	
	@Test
	public void when_loan_status_fail_then_check_fail() {
		_loanStatus = false;
		LoanChecker checker = new LoanChecker(
				_bankbalance,
				_creditStatus,
				_loanStatus,
				_income,
				_stockStatus);
		assertFalse(checker.check());
	}
	
	@Test
	public void when_stock_status_fail_then_check_fail() {
		_stockStatus = false;
		LoanChecker checker = new LoanChecker(
				_bankbalance,
				_creditStatus,
				_loanStatus,
				_income,
				_stockStatus);
		assertFalse(checker.check());
	}
}
