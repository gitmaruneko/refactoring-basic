/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.PullUpMethod_FormTemplateMethod.ans.step2;

public class LoanChecker extends FinancialChecker{

	@Override
	protected boolean checkBank() {
		return (_bankBalance >= 300000) ? true : false;
	}

	@Override
	protected boolean checkIncome() {
		return (_income >= 22000) ? true : false;
	}


	public LoanChecker(
			int bankBalance, 
			boolean creditStatus, 
			boolean loanStatus, 
			int income,
			boolean stockStatus){
		
		super(bankBalance, creditStatus, loanStatus, income, stockStatus);
	}

	
}
