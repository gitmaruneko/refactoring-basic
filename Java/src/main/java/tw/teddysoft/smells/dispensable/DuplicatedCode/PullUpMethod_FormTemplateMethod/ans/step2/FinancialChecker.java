/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.PullUpMethod_FormTemplateMethod.ans.step2;

public abstract class FinancialChecker {

	public final boolean check(){
        return checkBank() 
        		&& checkCredit()
        		&& checkLoan()
        		&& checkIncome()
        		&& checkStock(); 
	}
	
	protected abstract boolean checkBank();

	protected boolean checkCredit() {
		return _creditStatus;
	}

	protected boolean checkLoan() {
		return _loanStatus;
	}

	protected abstract boolean checkIncome();

	protected boolean checkStock() {
		return _stockStatus;
	}
	
	
	
	protected int _bankBalance;
	private boolean _creditStatus; 
	private boolean _loanStatus;
	protected int _income;
	private boolean _stockStatus;
	
	public FinancialChecker(
			int bankBalance, 
			boolean creditStatus, 
			boolean loanStatus, 
			int income,
			boolean stockStatus){
		
		_bankBalance = bankBalance;
		_creditStatus = creditStatus;
		_loanStatus = loanStatus;
		_income = income;
		_stockStatus = stockStatus;
	}
	
}
