/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.Comments.ExtractMethod.exercise;

public class ThermalController {
	private double _workingTemperatureReading;
	private boolean _isTurnOn;
	
	public ThermalController(double workingTemperatureReading, boolean isTurnOn){
		_workingTemperatureReading = workingTemperatureReading;
		_isTurnOn = isTurnOn;
	}

	public boolean speedUp(){
		// make sure the device is not overheat
		if (_isTurnOn && (_workingTemperatureReading * 0.998) < 55){
			sendCommand("0x5518");
			return true;
		}
		else
			return false;
	}	
	
	private void sendCommand(String cmd){
		// ...
	}
}
