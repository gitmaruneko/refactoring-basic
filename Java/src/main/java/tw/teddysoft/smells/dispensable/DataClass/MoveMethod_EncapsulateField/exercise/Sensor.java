/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DataClass.MoveMethod_EncapsulateField.exercise;

public class Sensor {
	public static final double CriticalHighLimit = 50;
	public String _name;
	public double _reading;
	
	public Sensor(String name, double reading){
		_name = name;
		_reading = reading;
	}
}
