/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DataClass.MoveMethod_EncapsulateField.ans;

public class Monitor {
	Sensor _sensor;
	
	public Monitor(Sensor sensor){
		_sensor = sensor;
	}
	
	public boolean isCritical() {
		return _sensor.isCritical();
	}
	
	// a lot of code related to Monitor here
}
