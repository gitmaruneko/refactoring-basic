/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.PullUpMethod_FormTemplateMethod.ans.step1;

public class LoanChecker extends FinancialChecker{

	@Override
	public boolean check() {
        return checkBank() 
        		&& checkCredit()
        		&& checkLoan()
        		&& checkIncome()
        		&& checkStock(); 
	}
	
	@Override
	protected boolean checkBank() {
		return (_bankBalance >= 300000) ? true : false;
	}

	private boolean checkIncome() {
		return (_income >= 22000) ? true : false;
	}
	
	private boolean checkStock() {
		return _stockStatus;
	}
	

	
	public LoanChecker(
			int bankBalance, 
			boolean creditStatus, 
			boolean loanStatus, 
			int income,
			boolean stockStatus){
		super(bankBalance, creditStatus, loanStatus, income, stockStatus);
	}

	
	
}
