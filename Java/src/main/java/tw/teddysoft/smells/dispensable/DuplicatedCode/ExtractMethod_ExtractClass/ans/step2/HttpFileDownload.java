/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.ExtractMethod_ExtractClass.ans.step2;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * HttpFileDownload
 * 
 */
public class HttpFileDownload {
	public static void download(String url, String localFileName) throws Exception
	{
		FileOutputStream fileOut = null;
		ByteArrayOutputStream byteOut = null;
		InputStream in = null;
		try {
			in = getInputStreamFromURL(url);
			fileOut = new FileOutputStream(localFileName);
			byteOut = new ByteArrayOutputStream();
			int read;
			while ((read = in.read()) != -1) {
				byteOut.write(read);
			}
			fileOut.write(byteOut.toByteArray());
		} catch(Exception e){
			FileUtil.delete(localFileName);
			throw e;
		} 
		finally {
			Cleaner.close(byteOut);
			Cleaner.close(fileOut);
			Cleaner.close(in);
		}
	}
	
	public static InputStream getInputStreamFromURL(String aAddress) throws MalformedURLException, IOException{
		URL url = new URL(aAddress);
		URLConnection conn = url.openConnection();
		conn.setConnectTimeout(0);
		conn.setReadTimeout(0);
		return conn.getInputStream();
	}
}
