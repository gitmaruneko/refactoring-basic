/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.PullUpMethod_FormTemplateMethod.ans.step1;

public abstract class FinancialChecker {

	public abstract boolean check(); 
	
	protected abstract boolean checkBank();

	protected boolean checkCredit() {
		return _creditStatus;
	}

	protected boolean checkLoan() {
		return _loanStatus;
	}
	
	public FinancialChecker(
			int bankBalance, 
			boolean creditStatus, 
			boolean loanStatus, 
			int income,
			boolean stockStatus){
		
		_bankBalance = bankBalance;
		_creditStatus = creditStatus;
		_loanStatus = loanStatus;
		_income = income;
		_stockStatus = stockStatus;
	}
	
	
	protected int _bankBalance;
	protected boolean _creditStatus; 
	protected boolean _loanStatus;
	protected int _income;
	protected boolean _stockStatus;
	
}
