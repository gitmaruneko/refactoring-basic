/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.ExtractMethod_ExtractClass.ans.step1;

import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileUtil {
	public static String getContent(File aF) throws IOException {
		StringBuffer content = new StringBuffer();
		FileReader fileReader = null;
		try {
			char[] readBuffer = new char[8192];
			int readCount;

			fileReader = new FileReader(aF);
			while ((readCount = fileReader.read(readBuffer)) > 0) {
				content.append(readBuffer, 0, readCount);
			}
		} 
		finally{
			close(fileReader);
		}
			return content.toString();
	}
	
	public static void close(Closeable closeable) {
		if (null != closeable) {
			try {
				closeable.close();
			} catch (IOException e) {
				// logger.error("Fails to close a Closeable object, exception {} ", e.getMessage());
			}
		}
	}
	
	
	public static boolean delete(String aFileName) throws IOException {
		File file = new File(aFileName);
		return delete(file);
	}

	private static boolean delete(File aResource) throws IOException {
		if (aResource.isDirectory()) {
			File[] childFiles = aResource.listFiles();

			for (int i = 0; i < childFiles.length; i++) {
				delete(childFiles[i]);
			}
		}
		return aResource.delete();
	}
}
