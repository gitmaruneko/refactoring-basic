/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.SpeculativeGenerality.CollapseHierarchy_InlineClass_RemoveParameter_RenameMethod.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class FileManipulateTest {

	@Test
	public void when_I_new_FileManipulate_I_got_a_platform_specific_instacne() {

		FileManipulate fm = new FileManipulate();
		assertTrue(fm.copy("./config.ini", "./bak/config.ini"));
	}
}
