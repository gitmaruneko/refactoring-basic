/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.PullUpMethod_FormTemplateMethod.exercise;

public class LoanChecker extends FinancialChecker{

	@Override
	public boolean check() {
        return checkBank() 
        		&& checkCredit()
        		&& checkLoan()
        		&& checkIncome()
        		&& checkStock(); 
	}
	
	private boolean checkBank() {
		return (_bankBalance >= 300000) ? true : false;
	}
	
	private boolean checkCredit() {
		return _creditStatus;
	}

	private boolean checkLoan() {
		return _loanStatus;
	}

	private boolean checkIncome() {
		return (_income >= 22000) ? true : false;
	}

	private boolean checkStock() {
		return _stockStatus;
	}
	
	public LoanChecker(
			int bankBalance, 
			boolean creditStatus, 
			boolean loanStatus, 
			int income,
			boolean stockStatus){
		
		_bankBalance = bankBalance;
		_creditStatus = creditStatus;
		_loanStatus = loanStatus;
		_income = income;
		_stockStatus = stockStatus;
	}
	
	private int _bankBalance;
	private boolean _creditStatus; 
	private boolean _loanStatus;
	private int _income;
	private boolean _stockStatus;

	
}
