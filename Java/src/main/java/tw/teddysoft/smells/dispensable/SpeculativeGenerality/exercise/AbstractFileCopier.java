/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.SpeculativeGenerality.exercise;

public abstract class AbstractFileCopier {

	public abstract boolean copy(String source, String dest);
}
