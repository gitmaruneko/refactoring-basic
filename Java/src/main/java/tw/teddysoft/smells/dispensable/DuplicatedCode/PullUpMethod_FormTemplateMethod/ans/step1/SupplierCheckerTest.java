/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
//TODO refactor this test case to remove duplicated code

package tw.teddysoft.smells.dispensable.DuplicatedCode.PullUpMethod_FormTemplateMethod.ans.step1;

import static org.junit.Assert.*;

import org.junit.Test;

public class SupplierCheckerTest {
	
	private int _bankBalance = 10000000;
	private boolean _creditStatus = true; 
	private boolean _loanStatus = true;
	
	@Test
	public void supplierCheckPass() {
		SupplierChecker checker = new SupplierChecker(
				_bankBalance,
				_creditStatus,
				_loanStatus);
		assertTrue(checker.check());
	}

	@Test
	public void when_bank_balance_less_than_10000000_then_check_fail() {
		_bankBalance = 9999999;
		SupplierChecker checker = new SupplierChecker(
				_bankBalance,
				_creditStatus,
				_loanStatus);
		assertFalse(checker.check());
	}
	
	@Test
	public void when_credit_status_fail_then_check_fail() {
		_creditStatus = false;
		SupplierChecker checker = new SupplierChecker(
				_bankBalance,
				_creditStatus,
				_loanStatus);
		assertFalse(checker.check());
	}
	
	@Test
	public void when_loan_status_fail_then_check_fail() {
		_loanStatus = false;
		SupplierChecker checker = new SupplierChecker(
				_bankBalance,
				_creditStatus,
				_loanStatus);
		assertFalse(checker.check());
	}
}
