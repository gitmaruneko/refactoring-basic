/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DataClass.EncapsulateCollection.exercise;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class PersonTest {
	
	private Person _teddy = new Person();
	private Set<Course> _set = new HashSet<Course>();
	/*
	 * Adapted from Martin Fowler's Refactoring book
	 */

	@Before
	public void when_I_exposes_a_collection_to_clients_then_clients_can_directly_modify_it() {
		_set.add(new Course("Java Programming", false));
		_set.add(new Course("Exception Handling Design", true));
		_teddy.setCourses(_set);
		
		assertEquals(2, _teddy.getCourses().size());
		
		Course refactoring = new Course("Refactoring", true);
		_teddy.getCourses().add(refactoring);
		_teddy.getCourses().add(new Course("Scrum in practice", false));
		assertEquals(4, _teddy.getCourses().size());
	
		_teddy.getCourses().remove(refactoring);
		assertEquals(3, _teddy.getCourses().size());
	}

	@Test
	public void when_I_exposes_a_collection_to_clients_then_clients_couple_with_unwanted_responsibilities() {
		assertEquals(1, findAdvancedCourse());
	}

	private int findAdvancedCourse(){
		int count = 0;
		for(Course course : _teddy.getCourses()){
			if (course.isAdvanced())
				count++;
		}
		return count;
	}
	
}
