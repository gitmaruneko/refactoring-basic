/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.LazyClass.InlineClass.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonTest {

	@Test
	public void simulate_person_client() {
		Person teddy = new Person();
		teddy.getOfficeTelephone().setAreaCode("02");
		teddy.getOfficeTelephone().setNumber("2311-6230");
		assertEquals("(02)2311-6230", teddy.getTelephoneNumber());
	}

}
