/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.LazyClass.CollapseHierarchy.ans;

import java.util.LinkedList;
import java.util.List;

public class Author {
	private String _firstName;
	private String _lastName;
	private List<Publication> publications;
	private String _primaryJobTitle; 
	
	public Author(String firstName, String lastName){
		_firstName = firstName;
		_lastName = lastName;
		publications = new LinkedList<>();
	}
	
	public int getPublicationNumber(){
		return publications.size();
	}
	
	public String getFirstName(){
		return _firstName;
	}
	
	public String getLastName(){
		return _lastName;
	}

	public void setPrimaryJabTitle(String primaryJobTitle) {
		_primaryJobTitle = primaryJobTitle;
	}

	public String getPrimaryJobTitle() {
		return _primaryJobTitle;
	}
}
