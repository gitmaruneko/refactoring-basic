package tw.teddysoft.kata.bowling.ans;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GameTest {
	private Game game;
	
	@Before
	public void setup() {
		game = new Game();
	}
	
	@Test
	public void testAllZeros() {
		rollMany(20, 0);
		assertEquals(0, game.score());
	}

	@Test
	public void testAllOnes() {
		rollMany(20, 1);
		assertEquals(20, game.score());
	}
	
	private void rollMany(int n, int pins) {
		for(int i = 0; i < n; i++) {
			game.roll(pins);
		}
	}
	
	@Test
	public void testOneSpare() {
		rollSpare();
		game.roll(3);
		rollMany(17, 0);
		assertEquals(16, game.score());
	}
	
	@Test
	public void testOneStrike() {
		rollStrike();
		game.roll(3);
		game.roll(4);
		rollMany(16, 0);
		assertEquals(24, game.score());
	}
	
	@Test
	public void testPerfectGame() {
		rollMany(12, 10);
		assertEquals(300, game.score());
	}	

	@Test
	public void testSpareAtTenthFrame() {
		rollMany(18, 0);
		rollSpare();
		rollMany(1, 5);
		assertEquals(15, game.score());
	}	
	
	private void rollSpare() {
		game.roll(3);
		game.roll(7);
	}
	
	private void rollStrike() {
		game.roll(10);
	}
}
