package tw.teddysoft.kata.tennis.take4.exercise;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}