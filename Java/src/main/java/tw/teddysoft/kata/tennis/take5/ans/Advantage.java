package tw.teddysoft.kata.tennis.take5.ans;


public class Advantage extends GameState {

    public Advantage(TennisGame5 game) {
        super(game);
    }

    @Override
    public String getScore() {
        return "Advantage " + (game.player1Score > game.player2Score ? game.player1Name : game.player2Name);
    }

}
