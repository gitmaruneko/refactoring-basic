package tw.teddysoft.kata.tennis.take4.exercise;

public class TennisGame4 implements TennisGame {

    private String[] SCORE_MAP = new String[]{"Love", "Fifteen", "Thirty", "Forty"};
    private int player2Score;
    private int player1Score;
    private String player1Name;
    private String player2Name;

    public TennisGame4(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getScore() {

        if (isBeforeDeuce())
            return (isTie()) ? SCORE_MAP[player1Score] + "-All" :
                    SCORE_MAP[player1Score] + "-" + SCORE_MAP[player2Score];

        if (isDeuce())
            return "Deuce";

        if(isAdvantage())
            return "Advantage " + (player1Score > player2Score ? player1Name : player2Name);

        if (isGameOver())
            return "Win for " + (player1Score > player2Score ? player1Name : player2Name);

        throw new RuntimeException("Infeasible path");
    }

    private boolean isBeforeDeuce(){
        return player1Score < 4 && player2Score < 4 && !(player1Score + player2Score == 6);
    }

    private boolean isTie(){
        return player1Score == player2Score;
    }

    private boolean isOneOfPlayerOverForty() {
        return player1Score >= 4 || player2Score >=4;
    }

    private boolean isGameOver() {
        return  (isOneOfPlayerOverForty() && Math.abs(player1Score - player2Score) >= 2);
    }

    private boolean isAdvantage() {
        return  (isOneOfPlayerOverForty() && Math.abs(player1Score - player2Score) == 1);
    }

    private boolean isDeuce() {
        return player1Score == player2Score && player1Score >= 3;
    }


    public void wonPoint(String playerName) {
        if (playerName.equals(this.player1Name))
            player1Score++;
        else if (playerName.equals(this.player2Name))
            player2Score++;
        else
            throw new RuntimeException("Cannot find player name: " + playerName);
    }
}
