package tw.teddysoft.kata.tennis.take3.ans;

public class TennisGame3 implements TennisGame {

    private int player2Score;
    private int player1Score;
    private String player1Name;
    private String player2Name;

    public TennisGame3(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getScore() {

        if (isBeforeDeuce()) {
            String[] scoreMap = new String[]{"Love", "Fifteen", "Thirty", "Forty"};
            return isTie() ? scoreMap[player1Score] + "-All" : scoreMap[player1Score] + "-" + scoreMap[player2Score];

        }
        else {
            if (isTie())
                return "Deuce";

            return isAdvantage() ? "Advantage " + getLeadingPlayerName() : "Win for " + getLeadingPlayerName();
        }
    }

    private String getLeadingPlayerName() {
        return player1Score > player2Score ? player1Name : player2Name;
    }

    private boolean isAdvantage() {
        return Math.abs(player1Score - player2Score) == 1;
    }

    private boolean isTie() {
        return player1Score == player2Score;
    }

    private boolean isBeforeDeuce(){
        return player1Score < 4 && player2Score < 4 && !(player1Score + player2Score == 6);
    }


    public void wonPoint(String playerName) {
        if (playerName.equals(this.player1Name))
            player1Score++;
        else if (playerName.equals(this.player2Name))
            player2Score++;
        else
            throw new RuntimeException("Cannot find player name: " + playerName);
    }

}
