package tw.teddysoft.kata.tennis.take3.ans;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}