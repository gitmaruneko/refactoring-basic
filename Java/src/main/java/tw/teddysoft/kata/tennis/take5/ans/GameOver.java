package tw.teddysoft.kata.tennis.take5.ans;


public class GameOver extends GameState {

    public GameOver(TennisGame5 game) {
        super(game);
    }


    @Override
    public String getScore() {
        return "Win for " + (game.player1Score > game.player2Score ? game.player1Name : game.player2Name);

    }

}
