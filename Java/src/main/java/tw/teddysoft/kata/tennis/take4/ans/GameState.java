package tw.teddysoft.kata.tennis.take4.ans;

public abstract class GameState {

    protected TennisGame4 game;

    public GameState(TennisGame4 game) {
        this.game = game;
    }

    abstract String getScore();

    abstract void nextState();
}
