package tw.teddysoft.kata.tennis.take1.ans;

public class TennisGame1 implements TennisGame {

    private String[] SCORE_MAP = new String[]{"Love", "Fifteen", "Thirty", "Forty"};
    private int player1Score = 0;
    private int player2Score = 0;
    private String player1Name;
    private String player2Name;

    public TennisGame1(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public void wonPoint(String playerName) {
        if (playerName.equals(this.player1Name))
            player1Score++;
        else if (playerName.equals(this.player2Name))
            player2Score++;
        else
            throw new RuntimeException("Cannot find player name: " + playerName);
    }

    public String getScore() {
        if (isTie())
        {
            return calcTieScore();
        }
        else if (isReadyToWin())
        {
            return calcWinScore();
        }
        else
        {
            return calcDifferentScoreUnderDeuce();
        }
    }

    private String calcDifferentScoreUnderDeuce() {
        return SCORE_MAP[player1Score] + "-" +
                SCORE_MAP[player2Score];
    }

    private String calcWinScore() {
        String playerName = player1Score > player2Score ? player1Name : player2Name;
        if (Math.abs(player1Score - player2Score) == 1){
            return "Advantage " + playerName;
        }
        return "Win for " + playerName;
    }

    private String calcTieScore() {
        if(player1Score >= 3)
            return "Deuce";
        else
            return SCORE_MAP[player1Score] + "-All";
    }

    private boolean isReadyToWin() {
        return player1Score >= 4 || player2Score >=4;
    }

    private boolean isTie() {
        return player1Score == player2Score;
    }
}
