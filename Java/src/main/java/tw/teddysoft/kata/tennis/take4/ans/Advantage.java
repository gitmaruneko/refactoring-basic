package tw.teddysoft.kata.tennis.take4.ans;

public class Advantage extends GameState {

    public Advantage(TennisGame4 game) {
        super(game);
    }

    @Override
    public String getScore() {
        return "Advantage " + (game.player1Score > game.player2Score ? game.player1Name : game.player2Name);
    }

    @Override
    public void nextState() {
        if (game.isDeuce())
            game.changeState(new Deuce(game));
        else if (game.isGameOver())
            game.changeState(new GameOver(game));

    }
}
