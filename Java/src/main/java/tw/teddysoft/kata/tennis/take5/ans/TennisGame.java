package tw.teddysoft.kata.tennis.take5.ans;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}