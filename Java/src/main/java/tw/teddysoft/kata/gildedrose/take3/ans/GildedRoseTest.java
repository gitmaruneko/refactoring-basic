package tw.teddysoft.kata.gildedrose.take3.ans;

import org.approvaltests.combinations.CombinationApprovals;
import org.junit.Test;

public class GildedRoseTest { 

    @Test
    public void updateQuality() throws Exception {
        CombinationApprovals.verifyAllCombinations(
                this::doUpdateQuality,
                new String [] {"foo", "Aged Brie",
                		"Backstage passes to a TAFKAL80ETC concert", 
                		"Sulfuras, Hand of Ragnaros"},
                new Integer[] {-1, 0, 2, 6, 11},
                new Integer[] {0, 1, 49, 50});
    }

    private String doUpdateQuality(String name, int sellIn, int quality) {
        Item[] items = new Item[] {Item.createItem(name, sellIn, quality)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        return items[0].toString();
    }
}
